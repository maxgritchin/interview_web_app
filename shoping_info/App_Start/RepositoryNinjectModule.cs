﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Ninject.Modules;
using shoping_info.Repositories.TestData;
using shoping_info.Repositories;

namespace shoping_info.App_Start
{
    public class RepositoryNinjectModule: NinjectModule
    {
        public override void Load()
        {
            this.Bind<IShopRepository>().To<DBRepository>();
        }
    }
}