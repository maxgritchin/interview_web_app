﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace shoping_info.Models
{
    public class Shop
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public int CityID { get; set; }
    }
}