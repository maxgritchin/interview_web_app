﻿using System;
using shoping_info.Models;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace shoping_info.Repositories.DAL
{
    public class ShopContext: DbContext
    {
        public ShopContext()
            : base("ShopContext")
        {
        }

        public DbSet<Shop> Shops { get; set; }
        public DbSet<City> Cities { get; set; }
        public DbSet<Product> Products { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }
    }
}