﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using shoping_info.Models;

namespace shoping_info.Repositories
{
    /// <summary>
    /// интерфейс получения данных о магазинах
    /// </summary>
    public interface IShopRepository
    {
        List<Shop> GetShopsByFilter(string name, int startIndex, int count);
        int GetShopsCountByFilter(string name);
        Shop AddNewShop(Shop shop);
        List<City> GetAllCities();
        void DeleteShop(Shop shop);
        void UpdateShop(Shop shop);
        //
        List<Product> GetProductListOfShop(int shopID);
        void DeleteProduct(Product product);
        void UpdateProduct(Product product);
        Product AddNewProduct(Product product);
    }
}