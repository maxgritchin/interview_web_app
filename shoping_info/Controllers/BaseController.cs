﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using shoping_info.Repositories.TestData;
using shoping_info.Models;
using shoping_info.Repositories;
using Ninject;

namespace shoping_info.Controllers
{
    public class BaseController : Controller
    {
        protected IShopRepository _shopRepository;

        /// <summary>
        /// dependency injection
        /// </summary>
        public BaseController()
        {
            
            _shopRepository = MvcApplication.AppKernel.Get<IShopRepository>();
        }

        /// <summary>
        /// Динамическая загрузка данных 
        /// </summary>
        /// <param name="name">фильтр по имени магазина</param>
        /// <param name="jtStartIndex">начальная позиция</param>
        /// <param name="jtPageSize">количество элементов на странице</param>
        /// <param name="jtSorting">сортировка</param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult ShopListByFilter(string name = "", int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
        {
            try
            {
                // get all shops 
                var allShops = _shopRepository.GetShopsByFilter(name, jtStartIndex, jtPageSize);
                // get total count of shops in the system 
                int totalCountShops = _shopRepository.GetShopsCountByFilter(name);

                return Json(new { Result = "OK", Records = allShops, TotalRecordCount = totalCountShops });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }

        [HttpPost]
        public JsonResult AddNewShop(Shop shop)
        {
            try
            {
                var newShop = _shopRepository.AddNewShop(shop);
                return Json(new { Result = "OK", Record = newShop });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }

        [HttpPost]
        public JsonResult GetCitiesOption()
        {
            try
            {
                var cities = _shopRepository.GetAllCities().Select(c => new { DisplayText = c.Name, Value = c.ID });
                return Json(new { Result = "OK", Options = cities });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }

        [HttpPost]
        public JsonResult DeleteShop(Shop shop)
        {
            try
            {
                _shopRepository.DeleteShop(shop);
                return Json(new { Result = "OK" });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }

        [HttpPost]
        public JsonResult UpdateShop(Shop shop)
        {
            try
            {
                _shopRepository.UpdateShop(shop);
                return Json(new { Result = "OK" });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }

        [HttpPost]
        public JsonResult ProductList(int shopID)
        {
            try
            {
                var products = _shopRepository.GetProductListOfShop(shopID);
                return Json(new { Result = "OK", Records = products });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }

        [HttpPost]
        public JsonResult UpdateProduct(Product product)
        {
            try
            {
                _shopRepository.UpdateProduct(product);
                return Json(new { Result = "OK" });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }

        [HttpPost]
        public JsonResult DeleteProduct(Product product)
        {
            try
            {
                _shopRepository.DeleteProduct(product);
                return Json(new { Result = "OK" });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }

        [HttpPost]
        public JsonResult AddNewProduct(Product product)
        {
            try
            {
                var newProduct = _shopRepository.AddNewProduct(product);
                return Json(new { Result = "OK", Record = newProduct });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }
    }
}
