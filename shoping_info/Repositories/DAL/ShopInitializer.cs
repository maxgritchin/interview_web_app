﻿using System;
using System.Collections.Generic;
using System.Linq;
using shoping_info.Models;

namespace shoping_info.Repositories.DAL
{
    public class ShopInitializer : System.Data.Entity.DropCreateDatabaseIfModelChanges<ShopContext>
    {
        protected override void Seed(ShopContext context)
        {
            var cities = new List<City> 
            {
                new City {ID=1, Name="Moscow"},
                new City {ID=2, Name="Vladivostok"},
                new City {ID=3, Name="Krasnodar"},
                new City {ID=4, Name="Sochi"},
            };
            cities.ForEach(c => context.Cities.Add(c));
            context.SaveChanges();

            var shops = new List<Shop> 
            {
                new Shop {ID=1, Name="IT-Shop", CityID=1, Email="it@shop.ru"},
                new Shop {ID=2, Name="Tech-Shop", CityID=2, Email="tech@shop.ru"},
                new Shop {ID=2, Name="Food-Shop", CityID=3, Email="food@shop.ru"},
                new Shop {ID=2, Name="Sport-Shop", CityID=4, Email="sport@shop.ru"},
              };
            shops.ForEach(s => context.Shops.Add(s));
            context.SaveChanges();

            var products = new List<Product>
            {
                new Product {ID=1, Name="1", Description="", Price=10.10m, ShopID=1},
                new Product {ID=2, Name="2", Description="", ShopID=1},
                new Product {ID=3, Name="3", Description="", Price=500, ShopID=1},
                new Product {ID=4, Name="444", Description="", Price=2.50m, ShopID=2},
                new Product {ID=5, Name="555", Description="", Price=123, ShopID=2},
                new Product {ID=6, Name="666", Description="", Price=12, ShopID=2},
                new Product {ID=7, Name="7777", Description="", ShopID=3},
                new Product {ID=8, Name="8888", Description="", Price=100, ShopID=3},
                new Product {ID=9, Name="9998", Description="", Price=4.50m, ShopID=3},
                new Product {ID=10, Name="1000000", Description="", Price=1.50m, ShopID=4},
            };
            products.ForEach(p => context.Products.Add(p));
            context.SaveChanges();
        }
    }
}