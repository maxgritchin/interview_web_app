﻿using System;
using System.Linq;
using System.Collections.Generic;
using shoping_info.Repositories;
using shoping_info.Models;
using shoping_info.Repositories.DAL;

namespace shoping_info.Repositories.TestData
{
    public class DBRepository: IShopRepository
    {

        private ShopContext shopContext;

        public DBRepository()
        {
            shopContext = new ShopContext();
        }

        /// <summary>
        /// получения списка всех магазинов
        /// </summary>
        /// <returns>список магазинов</returns>
        public List<Shop> GetShopsByFilter(string name, int startIndex, int count)
        {
            IEnumerable<Shop> query = shopContext.Shops;

            //Filters
            if (!string.IsNullOrEmpty(name))
            {
                query = query.Where(p => p.Name.IndexOf(name, StringComparison.OrdinalIgnoreCase) >= 0);
            }

            return count > 0
               ? query.Skip(startIndex).Take(count).ToList() //Paging
               : query.ToList(); //No paging
        }

        /// <summary>
        /// получение количества магазинов в системе
        /// </summary>
        /// <returns></returns>
        public int GetShopsCountByFilter(string name)
        {
            IEnumerable<Shop> query = shopContext.Shops;

            //Filters
            if (!string.IsNullOrEmpty(name))
            {
                query = query.Where(p => p.Name.IndexOf(name, StringComparison.OrdinalIgnoreCase) >= 0);
            }

            return query.Count();
        }

        /// <summary>
        /// добавление нового магазина 
        /// </summary>
        /// <param name="shop">данные нового магазина</param>
        /// <returns>добавленный магазин</returns>
        public Shop AddNewShop(Shop shop)
        {
            List<Shop> shops = shopContext.Shops.ToList();
            int newID = 1;
            if (shops.Count > 0)
                newID = shops.OrderBy(s => s.ID).Last().ID + 1;
            shop.ID = newID;
            var newShop = shopContext.Shops.Add(shop);
            shopContext.SaveChanges();
            return newShop; 
        }

        /// <summary>
        /// получение списка городов
        /// </summary>
        /// <returns>список городов</returns>
        public List<City> GetAllCities()
        {
            return shopContext.Cities.OrderBy(c => c.Name).ToList();
        }

        /// <summary>
        /// удалить магазин из системы 
        /// </summary>
        /// <param name="shopID">идентификатор магазина</param>
        public void DeleteShop(Shop shop)
        {
            // remove products 
            List<Product> product = shopContext.Products.ToList();
            product.ForEach(p => shopContext.Products.Remove(p));
            // remove shop
            var deletedShop = shopContext.Shops.FirstOrDefault(s => s.ID == shop.ID);
            // save change
            shopContext.Shops.Remove(deletedShop);
            shopContext.SaveChanges();
        }

        /// <summary>
        /// обновить данные магазина
        /// </summary>
        /// <param name="shop">идентификатор магазина</param>
        public void UpdateShop(Shop shop)
        {
            var updatedShop = shopContext.Shops.FirstOrDefault(s => s.ID == shop.ID);
            if (updatedShop != null)
            {
                updatedShop.CityID = shop.CityID;
                updatedShop.Email = shop.Email;
                updatedShop.Name = shop.Name;
                // save change 
                shopContext.SaveChanges();
            }
        }

        /// <summary>
        /// получение списка продуктов для магазина
        /// </summary>
        /// <param name="shopID">идентификатор магазина</param>
        /// <returns>список продуктов</returns>
        public List<Product> GetProductListOfShop(int shopID)
        {
            List<Product> query = shopContext.Products.ToList();
            return query.Where(p => p.ShopID == shopID).ToList();
        }

        /// <summary>
        /// добавление нового продукта
        /// </summary>
        /// <param name="product">данные нового продукта</param>
        /// <returns>добавленный продукт</returns>
        public Product AddNewProduct(Product product)
        {
            List<Product> products = shopContext.Products.ToList();
            int newID = 1; 
            if (products.Count > 0) 
                newID = products.OrderBy(p => p.ID).Last().ID + 1;
            product.ID = newID;
            var newProduct = shopContext.Products.Add(product);
            shopContext.SaveChanges();
            return product; 
        }

        /// <summary>
        /// удалить продукт из системы 
        /// </summary>
        /// <param name="productID">идентификатор продукта</param>
        public void DeleteProduct(Product product)
        {
            var query = shopContext.Products.FirstOrDefault(p => p.ID == product.ID);
            shopContext.Products.Remove(query);
            shopContext.SaveChanges();
        }

        /// <summary>
        /// обновить данные продукта
        /// </summary>
        /// <param name="product">идентификатор продукта</param>
        public void UpdateProduct(Product product)
        {
            var updatedProduct = shopContext.Products.FirstOrDefault(p => p.ID == product.ID);
            if (updatedProduct != null)
            {
                updatedProduct.Name = product.Name;
                updatedProduct.Price = product.Price;
                updatedProduct.ShopID = product.ShopID;
                updatedProduct.Description = product.Description;
                // save change 
                shopContext.SaveChanges();
            }
        }

    }
}