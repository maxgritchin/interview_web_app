﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using Ninject;
using shoping_info.App_Start;
using System.Data.Entity;
using shoping_info.Repositories.DAL;

namespace shoping_info
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801

    public class MvcApplication : HttpApplication 
    {
        public static IKernel AppKernel;

        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();

            AppKernel = new StandardKernel(new RepositoryNinjectModule());

            WebApiConfig.Register(GlobalConfiguration.Configuration);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            Database.SetInitializer(new ShopInitializer());
            ShopContext db = new ShopContext();
            db.Database.Initialize(true);
        }
    }
}