﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using shoping_info.Repositories;
using shoping_info.Models;

namespace shoping_info.Repositories.TestData
{
    public class DataShopRepository: IShopRepository 
    {
        private List<Shop> _shops;
        private List<City> _cities;
        private List<Product> _products;
        
        public DataShopRepository()
        {
            _cities = new List<City>
            {
                new City {ID=1, Name="Moscow"},
                new City {ID=2, Name="Vladivostok"},
            };
            
            _shops = new List<Shop>
            {
                new Shop {ID=1, Name="IT-Shop", CityID=1, Email="it@shop.ru"},
                new Shop {ID=2, Name="Tech", CityID=2, Email="tech@shop.ru"},
            };

            _products = new List<Product>
            {
                new Product {ID=1, Name="111", Description="", Price=10.10m, ShopID=1},
                new Product {ID=2, Name="222", Description="", ShopID=1},
                new Product {ID=3, Name="333", Description="", Price=500, ShopID=1},
                new Product {ID=4, Name="444", Description="", Price=2.50m, ShopID=1},
                new Product {ID=5, Name="555", Description="", Price=123, ShopID=1},
                new Product {ID=6, Name="666", Description="", Price=12, ShopID=2},
                new Product {ID=7, Name="777", Description="", ShopID=2},
                new Product {ID=8, Name="888", Description="", Price=100, ShopID=2},
                new Product {ID=9, Name="999", Description="", Price=4.50m, ShopID=2},
            };

            
        }
        
        /// <summary>
        /// получения списка всех магазинов
        /// </summary>
        /// <returns>список магазинов</returns>
        public List<Shop> GetShopsByFilter(string name, int startIndex, int count)
        {
            IEnumerable<Shop> query = _shops;

            //Filters
            if (!string.IsNullOrEmpty(name))
            {
                query = query.Where(p => p.Name.IndexOf(name, StringComparison.OrdinalIgnoreCase) >= 0);
            }

            return count > 0
               ? query.Skip(startIndex).Take(count).ToList() //Paging
               : query.ToList(); //No paging
        }

        /// <summary>
        /// получение количества магазинов в системе
        /// </summary>
        /// <returns></returns>
        public int GetShopsCountByFilter(string name)
        {
            IEnumerable<Shop> query = _shops;

            //Filters
            if (!string.IsNullOrEmpty(name))
            {
                query = query.Where(p => p.Name.IndexOf(name, StringComparison.OrdinalIgnoreCase) >= 0);
            }

            return query.Count();
        }

        /// <summary>
        /// добавление нового магазина 
        /// </summary>
        /// <param name="shop">данные нового магазина</param>
        /// <returns>добавленный магазин</returns>
        public Shop AddNewShop(Shop shop)
        {
            shop.ID = _shops.Count > 0 ? _shops[_shops.Count - 1].ID + 1 : 1;
            _shops.Add(shop);
            return shop;
        }

        /// <summary>
        /// получение списка городов
        /// </summary>
        /// <returns>список городов</returns>
        public List<City> GetAllCities()
        {
            return _cities.OrderBy(c => c.Name).ToList();
        }

        /// <summary>
        /// удалить магазин из системы 
        /// </summary>
        /// <param name="shopID">идентификатор магазина</param>
        public void DeleteShop(Shop shop)
        {
            _shops.RemoveAll(s => s.ID == shop.ID);
        }

        /// <summary>
        /// обновить данные магазина
        /// </summary>
        /// <param name="shop">идентификатор магазина</param>
        public void UpdateShop(Shop shop)
        {
            var updatedShop = _shops.FirstOrDefault(s => s.ID == shop.ID);
            if (updatedShop != null)
            {
                updatedShop.CityID = shop.CityID;
                updatedShop.Email = shop.Email;
                updatedShop.Name = shop.Name;
            }
        }

        /// <summary>
        /// получение списка продуктов для магазина
        /// </summary>
        /// <param name="shopID">идентификатор магазина</param>
        /// <returns>список продуктов</returns>
        public List<Product> GetProductListOfShop(int shopID)
        {
            return _products.Where(p => p.ShopID == shopID).ToList();
        }

        /// <summary>
        /// добавление нового продукта
        /// </summary>
        /// <param name="product">данные нового продукта</param>
        /// <returns>добавленный продукт</returns>
        public Product AddNewProduct(Product product)
        {
            //product.ID = _shops.Count > 0 ? _shops[_shops.Count - 1].ID + 1 : 1;
            //_shops.Add(shop);
            return product;
        }

        /// <summary>
        /// удалить продукт из системы 
        /// </summary>
        /// <param name="productID">идентификатор продукта</param>
        public void DeleteProduct(Product product)
        {
            _products.RemoveAll(p => p.ID == product.ID);
        }

        /// <summary>
        /// обновить данные продукта
        /// </summary>
        /// <param name="product">идентификатор продукта</param>
        public void UpdateProduct(Product product)
        {
            var updatedProduct = _products.FirstOrDefault(p => p.ID == product.ID);
            if (updatedProduct != null)
            {
                updatedProduct.Name = product.Name;
                updatedProduct.Price = product.Price;
                updatedProduct.ShopID = product.ShopID;
            }
        }

    }
}